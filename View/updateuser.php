
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Form</title>
	<link rel="stylesheet" href="../Assets/style1.css">
	<script src="../Assets/jquery-3.4.1.min.js"></script>
</head>
<body>
	<div>
		<form action="updatedata.php" onsubmit="return javascript()" 
			class="form1" method="post" enctype="multipart/form-data">
			<h1>Registration Form</h1>

			
			<input type="text" name="fname" value="<?php echo $dr['first_name']; ?>" placeholder="First name" id="fname">
			<p id="fname_error">*Enter First name</p>
			<input type="text" name="lname" value="<?php echo $dr['last_name']; ?>" placeholder="Last name" id="lname">
			<p id="lname_error">*Enter Last name</p>

			<center>Gender:
            <label>Male<input type="radio" name="gender" value="male" id="male" required <?php echo $dr['gender'] == 'male' ? 'checked': ''; ?> ></label>            
            <label>Female<input type="radio" name="gender" value="female" id="male" <?php echo $dr['gender'] == 'female' ? 'checked': ''; ?>></label>	
			
			<?php $hobby= explode(" ", $dr['hobby']); ?>
			<br>Hobby:
			<label>Playing Cricket<input type="checkbox" name="hobby[]" value="Playing Cricket" <?php  echo (in_array("Playing", $hobby)  ? 'checked' : ''); ?> id="hobby" ></label>
			<label>Singing<input type="checkbox" name="hobby[]" value="Singing" <?php  echo (in_array("Singing", $hobby)  ? 'checked' : ''); ?> id="hobby"></label>
			<label>Reading<input type="checkbox" name="hobby[]" value="Reading" <?php  echo (in_array("Reading", $hobby)  ? 'checked' : ''); ?> id="hobby"></label>     

			</center>       		
            
           
			<input type="number" name="number" value="<?php echo $dr['mobile_number']; ?>" placeholder="Mobile number" maxlength="10" id="number">
			<p id="number_error">*mobile is not valid</p>
			<input type="email" name="Email" value="<?php echo $dr['email']; ?>" placeholder="Email" id="email">
			<p id="email_error">*email is not valid</p>
			<input type="password" name="password" placeholder="Password" id="password">
			<p id="password_error">*password not valid</p>
			<input type="password" name="conform_password"  placeholder="Confirm password" id="confirm_password">
			<input type="file" name="uploadImage" style="margin-left: 45px;">
			<center><input type="submit" name="btn" class="btn"></center>
		</form>
	</div>
</body>
<script>
	     $(document).ready(function(){
	     	$("p").hide();

	      	$("#fname").blur(function(){
	      		var fname = $(this).val();
	      		if(fname == '')
	      		{
	      			$("#fname_error").show();
	      		}
	      		else
	      		{
	      			$("#fname_error").hide();
	      		}
	      	});

	      	$("#lname").blur(function(){
	      		var lname = $(this).val();
	      		if(lname == '')
	      		{
	      			$("#lname_error").show();
	      		}
	      		else
	      		{
	      			$("#lname_error").hide();
	      		}
	      	});


	      	$("#number").blur(function(){
	      		var number = $(this).val();
	      		if(number == '')
	      		{
	      			$("#number_error").show();
	      		}
	      		else if(number.length != 10)
	      		{
	      			$("#number_error").show();
	      		}
	      		else
	      		{
	      			$("#number_error").hide();
	      		}
	      	});


	      	$("#email").blur(function(){
	      		var email = $(this).val();
	      		var email_len = email.split('.').reverse();
	      		if(email == '')
	      		{
	      			$("#email_error").show();
	      		}
	      		else if(email_len[0].length > 3)
	      		{
	      			$("#email_error").show();
	      		}
	      		else
	      		{
	      			$("#email_error").hide();
	      		}
	      	});


	      	$("#password").blur(function(){
	      		var password = $(this).val();
	      		if(password != '')
	      		{
	      			if(password.length < 8)
	      			{
	      			  $("#password_error").show();
	      		    }
                    
                     re= /[0-9]/;
	      		    if(!re.test(password))
	      			{
	      			  $("#password_error").show();
	      		    }
                     
				     re= /[a-z]/;
	      		    if(!re.test(password))
	      			{
	      			  $("#password_error").show();
	      		    }
                    
				     re= /[A-Z]/;
                     if(!re.test(password))
	      			{
	      			  $("#password_error").show();
	      		    }

				     re= /[@$%_^]/;
          		    if(!re.test(password))
          			{
	      			  $("#password_error").show();
	      		    }
	     		    
	      		    else
	      		    {
	      			  $("#password_error").hide();
	      		    }
	      		}
	      		else
	      		{
	      			$("#password_error").show();
	      		}

	      	});

	      	$("confirm_password").blur(function(){
	      		var pass2 = $(this).val();
	      		var pass1 = $("#pass1").val();

	      		if(pass1 != pass2)
	      		{
	      			$("confirm_password").show();
	      		}
	      		else
	      		{
	      			$("confirm_password").hide();
	      		}
	      	});

	      	$(".btn").click(function(){

	      		var gender=$("#male:checked").val();
	      		var hobby = [];
	      		$.each($("input[name='hobby']:checked"),function(){
	      			hobby.push($(this).val());
	      		});

	      		var number =$("#number").val();
	      		var email =$("#email").val();
			 	var fname =$("#fname").val();
				 var lname =$("#lname").val();
				var select =$(".select").val();
				var add =$(".txt").val();
				var password =$("#password").val();
				});
	     });
</script>
</html>